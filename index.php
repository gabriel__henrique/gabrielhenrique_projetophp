<?
// error_reporting(E_ERROR | E_PARSE);
require ("uteis.php");
$user = new Restrito;
if(!$user->acesso()){
    header('Location: login.php');
}
if($_GET['page'] == 'logout'){
    if($user->logout()){
        header('Location: '.$urlSite.'login.php');
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?=$urlSite?>css/bootstrap.min.css.">
    <link rel="stylesheet" href="<?=$urlSite?>css/app.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Document</title>
</head>
<body class="pb-5">
<nav class="navbar navbar-expand-lg navbar-dark bg-purple container-fluid">
  <a class="navbar-brand bg-purple" href="#">Navbar</a>
  <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon "></span>
  </button>

  <div class="collapse navbar-collapse " id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto bg-purple">   
        <?foreach($nav as $ch=>$menu){
            if(is_array($menu)){?>
                <li class="nav-item dropdown bg-purple">
                    <a class="nav-link dropdown-toggle bg-purple" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">
                    <?=$ch?>
                    </a>
                    <div class="dropdown-menu bg-purple" aria-labelledby="navbarDropdown">
                <? foreach($menu as $ch2=>$submenu){ ?>
                    <a class="dropdown-item bg-purple" href="<?=$urlSite.$ch2?>"><?=$submenu?></a>
                    <? } ?>
                </div>
                </li>
           <? }else{?>
            <li class="nav-item bg-purple">
                <a class="nav-link bg-purple" href="<?=$urlSite.$ch?>"><?=$menu?> </a>
            </li>
         <? } ?>
    <? } ?>
</ul>
<?
$nome = explode(' ',$_SESSION['USUARIO']['nome'])
?>
<ul class="navbar-nav bg-purple" >
    <li class="nav-item bg-purple" style="width: 25px;"><a href="<?=$urlSite?>logout" class="navbar-link text-light" ><i class="bi bi-box-arrow-in-right" ></i></a></li>
    <li class="nav-item bg-purple"><small class="ml-5 text-light text-center">Olá <?=$nome[0]?>, seja bem vindo</small></li>
</ul>
</div>

</nav>
    <main class="container mt-5">
    <?
    switch ($_GET['page']) {
        case '':
        case 'home':
            require "controllers/home.php";
            require "views/home.php";
            break;
        
        default:
            require 'controllers/'.$_GET['page'].'.php';
            require 'views/'.$_GET['page'].'.php';
            break;
    } 
    // session_destroy();   
    ?>

    </main>
    <footer class="container-fluid bg-purple">
        <div class="row">
            <div class="col-sm-4 text-center text-light">&copy; Todos os direitos reservados</div>
            <div class="col-sm-4 text-right">
                <a href="https://api.whatsapp.com/send?phone=+5547991594765"><i class="bi bi-whatsapp text-light"> Suporte</i></a>
            </div>
        </div>
        
    </footer>
    <script>var urlSite = '<?=$urlSite?>'</script>
    <script src="<?=$urlSite?>js/jquery-3.6.0.min.js"></script>
    <script src="<?=$urlSite?>js/jquery.mask.min.js"></script>
    <script src="<?=$urlSite?>js/app.js?=<?=rand(0,9999)?>"></script>
    <script src="<?=$urlSite?>js/bootstrap.bundle.min.js"></script>
</body>
</html>