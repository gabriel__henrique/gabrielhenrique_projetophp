<?
Class Bloco extends Condominio{

    function __construct(){

    }
    function getBloco($id = null){
      $qry = 'SELECT
      b.id,
      cond.id AS idCond,
      cond.nomeCondominio,
      b.nomeBloco,
      b.qtAndar,
      b.qtAptoAndar
      FROM bloco b
      INNER JOIN condominio cond ON cond.id = b.idCondominio ';
      $contaTermos = count($this->busca);
      if($contaTermos > 0){
        $i = 0;
        foreach($this->busca as $field=>$termo){
            if($i ==0 && $termo!=null){
                $qry = $qry.' WHERE ';
                $i++;
            }
            switch ($termo) {
                case is_numeric($termo):
                    if(!empty($termo)){
                        $qry = $qry.' '.$field.' = '.$termo.' AND ';
                    }
                    break;
                        
                    default:
                    if(!empty($termo)){
                         $qry = $qry.$field.' LIKE "%'.$termo.'%"'.' AND ';
                    }
                    break;
            }
        }
        $qry = rtrim($qry, ' AND ');
    }
      if($id){
          $qry .= ' WHERE b.id ='.$id;
          $unic = true;
      }
      $qry .= '
      ORDER BY cond.nomeCondominio asc ';
      return $this->listarData($qry, $unic);
    }

    function getBlocoFromCondominio($idCondominio){
      $qry = 'SELECT id, nomeBloco FROM bloco WHERE idCondominio = '.$idCondominio;
      return $this->listarData($qry);
    }

    function addBloco($dados){
      $values ='';
        $qry = 'INSERT INTO bloco (';
        foreach($dados as $ch=> $value){
            $qry .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .= ') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);
    }
    function editarBloco($dados){
     
      $qry = "UPDATE  bloco SET";
      foreach($dados as $ch=> $value){
          if($ch !='editar'){
              $qry .="`".$ch."` = '".$value."', ";
          }
      }
      $qry = rtrim($qry,', ');
      $qry .=' WHERE id='.$dados['editar'];
      return $this->updateData($qry);
    }
    function deletarBloco($id){
      $qry = 'DELETE FROM bloco WHERE id ='.$id;
      return $this->deletar($qry);
    }
}
?>