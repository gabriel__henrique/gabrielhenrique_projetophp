<?
Class Conselho extends Condominio{
    function __construct(){

    }

    function getConselho($id = null){
      $qry = 'SELECT
      con.id,
      cond.id AS idCondominio,
      cond.nomeCondominio,
      con.nome AS Nome,
      con.funcao AS funcao
      FROM conselho con
      LEFT JOIN condominio cond ON cond.id = con.idCondominio';
      $contaTermos = count($this->busca);
      if($contaTermos > 0){
      $i = 0;
      foreach($this->busca as $field=>$termo){
        if($i ==0 && $termo!=null){
        $qry = $qry.' WHERE ';
        $i++;
        }
        switch ($termo) {
          case is_numeric($termo):
            if(!empty($termo)){
              $qry = $qry.' '.$field.' = '.$termo.' AND ';
            }
            break;
                              
            default:
            if(!empty($termo)){
              $qry = $qry.$field.' LIKE "%'.$termo.'%"'.' AND ';
            }
            break;
        }
      }
      $qry = rtrim($qry, ' AND ');
  }
      if($id){
          $qry .= ' WHERE con.id ='.$id;
          $unic = true;
      }
      $qry .= '
      ORDER BY cond.nomeCondominio ASC ';
      return $this->listarData($qry, $unic);
  }

    function addConselho($dados){
      $values ='';
      $qry = 'INSERT INTO conselho (';
      foreach($dados as $ch=> $value){
          $qry .='`'.$ch.'`, ';
          $values .= "'".$value."', ";
      }
      $qry = rtrim($qry,', ');
      $qry .= ') VALUES ('.rtrim($values,', ').')';
      return $this->insertData($qry);
  }       

    function editarConselho($dados){
      
      $qry = "UPDATE  conselho SET";
      foreach($dados as $ch=> $value){
          if($ch !='editar'){
              $qry .="`".$ch."` = '".$value."', ";
          }
      }
      $qry = rtrim($qry,', ');
      $qry .=' WHERE id='.$dados['editar'];
      return $this->updateData($qry);
  }

    function deletarConselho($id){
      $qry = 'DELETE FROM conselho WHERE id ='.$id;
      return $this->deletar($qry);
     }
}
?>