<?
Class Adm extends Dao{
  function __construct(){

  }

  function getAdministradora($id = null){
    $qry = 'SELECT id, nomeAdm, cnpj, dataCadastro FROM administradora';
    $contaTermos = count($this->busca);

        if($contaTermos > 0){
            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i ==0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.' '.$field.' = '.$termo.' AND ';
                        }
                        break;
                            
                        default:
                        if(!empty($termo)){
                             $qry = $qry.$field.' LIKE "%'.$termo.'%"'.' AND ';
                        }
                        break;
                }
            }
            $qry = rtrim($qry, ' AND ');
        }
    if($id){
      $unic = true;
      $qry .= ' WHERE id ='.$id;
    }
    return $this->listarData($qry,$unic);
  }

  function getUltimasAdm(){
    $qry = 'SELECT 
    nomeAdm
    FROM administradora
    ORDER BY dataCadastro DESC
    LIMIT 5';

    return $this->listarData($qry);
  }

  function addAdministradora($dados){
    $values ='';
    $qry = 'INSERT INTO administradora (';
    foreach($dados as $ch=> $value){
      $qry .='`'.$ch.'`, ';
      $values .= "'".$value."', ";
    }
    $qry = rtrim($qry,', ');
    $qry .= ') VALUES ('.rtrim($values,', ').')';
    return $this->insertData($qry);
  }      

  function editarAdministradora($dados){
      
    $qry = "UPDATE  administradora SET";
    foreach($dados as $ch=> $value){
      if($ch !='editar'){
        $qry .="`".$ch."` = '".$value."', ";
      }
    }
    $qry = rtrim($qry,', ');
    $qry .=' WHERE id='.$dados['editar'];
    return $this->updateData($qry);
  }

  function deletarAdministradora($id){
    $qry = 'DELETE FROM administradora WHERE id ='.$id;
    return $this->deletar($qry);
  }
}
?>