<?
Class Unidade extends Bloco{

    function __construct(){

    }
    function getUnidade($id = null){
        $qry = 'SELECT
        unid.id,
        unid.idCondominio,
        unid.idBloco,
        cond.nomeCondominio,
        b.nomeBloco ,
        unid.numero,
        unid.metragem,
        unid.qtVagas
        FROM unidade unid
        INNER JOIN condominio cond ON cond.id = unid.idCondominio
        INNER JOIN bloco b ON b.id = unid.idBloco';
                $contaTermos = count($this->busca);
                if($contaTermos > 0){
                    $i = 0;
                    foreach($this->busca as $field=>$termo){
                        if($i ==0 && $termo!=null){
                            $qry = $qry.' WHERE ';
                            $i++;
                        }
                        switch ($termo) {
                            case is_numeric($termo):
                                if(!empty($termo)){
                                    $qry = $qry.' '.$field.' = '.$termo.' AND ';
                                }
                                break;
                                    
                                default:
                                if(!empty($termo)){
                                     $qry = $qry.$field.' LIKE "%'.$termo.'%"'.' AND ';
                                }
                                break;
                        }
                    }
                    $qry = rtrim($qry, ' AND ');
                }
        if($id){
            $qry .= ' WHERE unid.id = '.$id;
            $unic = true;
        }
       
        return $this->listarData($qry,$unic);
    }
    function getUnidadeFromBloco($idBloco){
        $qry = 'SELECT id, numero FROM unidade WHERE idBloco = '.$idBloco;
        return $this->listarData($qry);
      }


    function addUnidade($dados){
        $values ='';
        $qry = 'INSERT INTO unidade (';
        foreach($dados as $ch=> $value){
            $qry .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .= ') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);
    }

    function editarUnidade($dados){

        $qry = "UPDATE  unidade SET";
        foreach($dados as $ch=> $value){
            if($ch !='editar'){
                $qry .="`".$ch."` = '".$value."', ";
            }
        }
        $qry = rtrim($qry,', ');
        $qry .=' WHERE id='.$dados['editar'];
        return $this->updateData($qry);
    }

    function deletarUnidade($id){
        $qry = 'DELETE FROM unidade WHERE id ='.$id;
        return $this->deletar($qry);
    }

}

?>