<?
Class Morador extends Unidade{

    function __construct(){

    }

    function getMorador($id = null){
        $qry = 'SELECT 
        mor.id,
        mor.idCondominio,
        
        mor.idBloco,
        mor.idUnidade,
        cond.nomeCondominio,
        b.nomeBloco,
        unid.numero,
        mor.nome,
        mor.cpf,
        mor.email,
        mor.telefone,
        mor.dataCadastro
        FROM morador mor
        INNER JOIN condominio cond ON cond.id = mor.idCondominio
        INNER JOIN bloco b ON b.id = mor.idBloco
        INNER JOIN unidade unid ON unid.id = mor.idUnidade ';
        $contaTermos = count($this->busca);

        if($contaTermos > 0){
            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i ==0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.' '.$field.' = '.$termo.' AND ';
                        }
                        break;
                            
                        default:
                        if(!empty($termo)){
                             $qry = $qry.$field.' LIKE "%'.$termo.'%"'.' AND ';
                        }
                        break;
                }
            }
            $qry = rtrim($qry, ' AND ');
        }
        if($id){
            $qry .= ' WHERE mor.id ='.$id;
            $unic = true;
        }
        
        
        return $this->listarData($qry, $unic);
    }
    
    function getMoradorporCondominio(){
        $qry = 'SELECT
        cond.nomeCondominio,
        COUNT(mor.id) as qtMoradores
        FROM morador mor 
        RIGHT JOIN condominio cond on cond.id = mor.idCondominio
        GROUP BY cond.id';

        
        return $this->listarData($qry);
    }

    function getTotalRegistros(){
        $qry = 'SELECT 
        count(mor.id) AS qtMoradores,
        (SELECT count(unid.id) FROM unidade unid) AS qtUnidades,
        (SELECT count(b.id) FROM bloco b) AS qtBlocos,
        (SELECT count(cond.id) FROM condominio cond) AS qtCondominios,
        (SELECT count(adm.id) FROM administradora adm)AS qtAdms 
        FROM morador mor';

        $unic =true;
        return $this->listarData($qry,$unic);
    }

    function addMorador($dados){
        $values ='';
        $qry = 'INSERT INTO morador (';
        foreach($dados as $ch=> $value){
            $qry .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .= ') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);
    }      

    function editarMorador($dados){

        $qry = "UPDATE  morador SET";
        foreach($dados as $ch=> $value){
            if($ch !='editar'){
                $qry .="`".$ch."` = '".$value."', ";
            }
        }
        $qry = rtrim($qry,', ');
        $qry .=' WHERE id='.$dados['editar'];
        return $this->updateData($qry);
    }

    function deletarMorador($id){

        $qry = 'DELETE FROM morador WHERE id ='.$id;
        return $this->deletar($qry);
    }
}
?>