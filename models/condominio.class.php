<?
Class Condominio extends Adm{

    function __construct(){

    }
    function getCondominio($id = null){
        $qry = 'SELECT
        cond.id,
        adm.id AS idAdm,
        adm.nomeAdm,
        cond.nomeCondominio,
        cond.qtBlocos,
        cond.cep,
        cond.logradouro,
        cond.numero,
        cond.bairro,
        cond.cidade,
        cond.estado
        FROM condominio cond
        INNER JOIN administradora adm ON adm.id = cond.idAdm ';
        $contaTermos = count($this->busca);
        if($contaTermos > 0){
            $i = 0;
            foreach($this->busca as $field=>$termo){
                if($i ==0 && $termo!=null){
                    $qry = $qry.' WHERE ';
                    $i++;
                }
                switch ($termo) {
                    case is_numeric($termo):
                        if(!empty($termo)){
                            $qry = $qry.' '.$field.' = '.$termo.' AND ';
                        }
                        break;
                            
                        default:
                        if(!empty($termo)){
                             $qry = $qry.$field.' LIKE "%'.$termo.'%"'.' AND ';
                        }
                        break;
                }
            }
            $qry = rtrim($qry, ' AND ');
        }
        if($id){
            $qry .= ' WHERE cond.id = '.$id;
            $unic = true;
        }
        $qry .= '
        ORDER BY adm.nomeAdm asc ';
        return $this->listarData($qry, $unic);
    }
    function getUltimosCond(){
        $qry = 'SELECT 
        nomeCondominio
        FROM condominio
        ORDER BY dataCadastro DESC
        LIMIT 5';
    
        return $this->listarData($qry);
      }
    

    function addCondominio($dados){
        $values ='';
        $qry = 'INSERT INTO condominio (';
        foreach($dados as $ch=> $value){
            $qry .='`'.$ch.'`, ';
            $values .= "'".$value."', ";
        }
        $qry = rtrim($qry,', ');
        $qry .= ') VALUES ('.rtrim($values,', ').')';
        return $this->insertData($qry);
    }

    function editarCondominio($dados){
        $values ='';

        $qry = "UPDATE  condominio SET";
        foreach($dados as $ch=> $value){
            if($ch !='editar'){
                $qry .="`".$ch."` = '".$value."', ";
            }
        }
        $qry = rtrim($qry,', ');
        $qry .=' WHERE id='.$dados['editar'];
        return $this->updateData($qry);
    }

    function deletarCondominio($id){
     $qry = 'DELETE FROM condominio WHERE id ='.$id;
     return $this->deletar($qry);
    }

}
?>