$(function(){
    $('.formUsuario').submit(function(){
        var senha = $(this).find('#senha').val();
        var confirmaSenha = $(this).find('#csenha').val();
        var editar = $(this).find('input[name="g[editar]"').val();
        var url;
        var urlRedir;

        if(editar){
            url = urlSite+'api/editaUsuario.php';
            urlRedir = urlSite+'listaUsuario';
        }else{
            url = urlSite+'api/cadastraUsuario.php';
            urlRedir = urlSite+'cadastroUsuario';
        }
        $('.buttonEnviar').attr('disabled', true);
        if(senha == confirmaSenha){
            $.ajax({
                url: url,
                dataType: 'json',
                type: 'POST',
                data: $(this).serialize(),
                success : function(data){
                    console.log(data);
                    
                    if(data.status == 'success'){
                        
                        myAlert(data.status, data.msg, 'main', urlRedir)
                    }else{
                        myAlert(data.status, data.msg, 'main', urlRedir);
                    }
                }
            });
        }else{
            myAlert('danger', 'senha não confere','main')
        }
            
        return false;
    });
    $('#listaUsuario').on('click','.removerUsuario', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: urlSite+'api/deletaUsuario.php',
            dataType: 'json',
            type: 'POST',
            data: {id: idRegistro},
            success : function(data){
                console.log(data)
                if(data.status == 'success'){
                    console.log (idRegistro)
                    myAlert(data.status, data.msg, 'main',urlSite+'listaUsuario')
                }else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })
    //form adm
    $('.formAdm').submit(function(){
        var editar = $(this).find('input[name="editarADM"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = urlSite+'api/editaAdm.php';
            urlRedir = urlSite+'listaADM';
        }else{
            url = urlSite+'api/cadastraAdm.php';
            urlRedir = urlSite+'cadastroADM';
        }
        $('.buttonEnviar').attr('disabled', true);

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                console.log(data);

                if(data.status == 'success'){
                    //$('.totalRegistro').html(data.totalRegistro)
                    myAlert(data.status, data.msg, 'main', urlRedir)
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });
        
        return false;
    });
    //deleta adm
    $('#listaAdm').on('click','.removerAdm', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: urlSite+'api/deletaAdm.php',
            dataType: 'json',
            type: 'POST',
            data: {id: idRegistro},
            success : function(data){
                console.log(data)
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main',urlSite+'listaADM')
                }else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })
    /////////////////////// morador
    $('.form-morador').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = urlSite+'api/editaMorador.php';
            urlRedir = urlSite+'listaMorador';
        }else{
            url = urlSite+'api/cadastraMorador.php';
            urlRedir = urlSite+'cadastroMorador';
        }
        $('.buttonEnviar').attr('disabled', true);

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                if(data.status == 'success'){
                    //$('.totalRegistro').html(data.totalRegistro)
                    myAlert(data.status, data.msg, 'main', urlRedir)
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });
        
        return false;
    });
    //deleta cliente
    $('#listaMorador').on('click','.removerMorador', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: urlSite+'api/deletaMorador.php',
            dataType: 'json',
            type: 'POST',
            data: {id: idRegistro},
            success : function(data){
                console.log(data)
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main',urlSite+'listaMorador')
                }else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })
    
    $('.formCondominio').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = urlSite+'api/editaCondominio.php';
            urlRedir = urlSite+'listaCondominio';
        }else{
            url = urlSite+'api/cadastraCondominio.php';
            urlRedir = urlSite+'cadastroCondominio';
        }
        $('.buttonEnviar').attr('disabled', true);

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                console.log(data);

                if(data.status == 'success'){
                    //$('.totalRegistro').html(data.totalRegistro)
                    myAlert(data.status, data.msg, 'main', urlRedir)
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });
        
        return false;
    });
    //deleta cliente
    $('#listaCondominio').on('click','.removerCondominio', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: urlSite+'api/deletarCondominio.php',
            dataType: 'json',
            type: 'POST',
            data: {id: idRegistro},
            success : function(data){
                console.log(data)
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main',urlSite+'listaCondominio')
                }else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })
    ////////////////////////////// bloco
    $('.formBloco').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = urlSite+'api/editaBloco.php';
            urlRedir = urlSite+'listaBloco';
        }else{
            url = urlSite+'api/cadastraBloco.php';
            urlRedir = urlSite+'cadastroBloco';
        }
        $('.buttonEnviar').attr('disabled', true);

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                console.log(data);

                if(data.status == 'success'){
                    //$('.totalRegistro').html(data.totalRegistro)
                    myAlert(data.status, data.msg, 'main', urlRedir)
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });
        
        return false;
    });
    //deleta Bloco
    $('#listaBloco').on('click','.removerBloco', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: urlSite+'api/deletaBloco.php',
            dataType: 'json',
            type: 'POST',
            data: {id: idRegistro},
            success : function(data){
                console.log(data)
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main',urlSite+'listaBloco')
                }else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })
    $('.formUnidade').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = urlSite+'api/editaUnidade.php';
            urlRedir = urlSite+'listaUnidade';
        }else{
            url = urlSite+'api/cadastraUnidade.php';
            urlRedir = urlSite+'cadastroUnidade';
        }
        $('.buttonEnviar').attr('disabled', true);

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                console.log(data);

                if(data.status == 'success'){
                    //$('.totalRegistro').html(data.totalRegistro)
                    myAlert(data.status, data.msg, 'main', urlRedir)
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });
        
        return false;
    });
    //deleta Bloco
    $('#listaUnidade').on('click','.removerUnidade', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: urlSite+'api/deletaUnidade.php',
            dataType: 'json',
            type: 'POST',
            data: {id: idRegistro},
            success : function(data){
                console.log(data)
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main',urlSite+'listaUnidade')
                }else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })
    $('.formConselho').submit(function(){
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;

        if(editar){
            url = urlSite+'api/editaConselho.php';
            urlRedir = urlSite+'listaConselho';
        }else{
            url = urlSite+'api/cadastraConselho.php';
            urlRedir = urlSite+'cadastroConselho';
        }
        $('.buttonEnviar').attr('disabled', true);

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success : function(data){
                console.log(data);

                if(data.status == 'success'){
                    //$('.totalRegistro').html(data.totalRegistro)
                    myAlert(data.status, data.msg, 'main', urlRedir)
                }else{
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        });
        
        return false;
    });
    //deleta Bloco
    $('#listaConselho').on('click','.removerConselho', function(){
        var idRegistro = $(this).attr('data-id');
        $.ajax({
            url: urlSite+'api/deletaConselho.php',
            dataType: 'json',
            type: 'POST',
            data: {id: idRegistro},
            success : function(data){
                console.log(data)
                if(data.status == 'success'){
                    myAlert(data.status, data.msg, 'main',urlSite+'listaConselho')
                }else{
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('#filtro').submit(function(){
        var pagina = $('input[name="page"]').val();
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();

        termo1 = (termo1) ? termo1+'/' : '';
        termo2 = (termo2) ? termo2+'/' : '';
        window.location.href = urlSite+pagina+'/busca/'+termo1+termo2
        return false
    })

    $('.termo1, .termo2').on('keyup focusout change',function(){
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();
        if(termo1 || termo2){
            $('button[type="submit"]').prop('disabled', false)
        }else{
            $('button[type="submit"]').prop('disabled', true)
        }
    })



    //mascaras
    $('input[name="cpf"]').mask('000.000.000-00')
    $('input[name="telefone"]').mask('(00) 00000-0000')
    $('input[name="CEP"]').mask('00000-000')

});
$(function(){
    $('.fromCondominio').change(function(){
        selecionado = $(this).val();

        $.ajax({
            url: urlSite+'api/listBloco.php',
            dataType: 'json',
            type: 'POST',
            data: {id: selecionado},
            success : function(data){
                selectPopulation('.fromBloco',data.resultSet, 'nomeBloco')
            }
        })
    })
    $('.fromBloco').change(function(){
        selecionado = $(this).val();

        $.ajax({
            url: urlSite+'api/listUnidade.php',
            dataType: 'json',
            type: 'POST',
            data: {id: selecionado},
            success : function(data){
                selectPopulation('.fromUnidade',data.resultSet, 'numero' )
            }
        })
    })

    function selectPopulation(seletor, dados, field){
        estrutura = '<option value="">Selecione...</option>';


            for (let i = 0; i < dados.length; i++){
                estrutura += '<option value="'+dados[i].id+'">'+dados[i][field]+'</option>'
            }


        $(seletor).html(estrutura);
    }
})
function myAlert(tipo, mensagem, pai, url){
    url = (url == undefined) ? url = '' : url = url;
      componente ='<div class="alert alert-'+tipo+'" role="alert">'+mensagem+'</div>';
      
      $(pai).prepend(componente);

      setTimeout(function(){
          $(pai).find('div.alert').remove();
          //redirecionar?
          if(tipo == 'success' && url){ 
              setTimeout(function(){
                  window.location.href = url;
              },500)
          }
      }, 3000)
  }
