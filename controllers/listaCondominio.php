<?
//busca adm
$adm = new Adm();
$listaAdm = $adm->getAdministradora();

//busca condominio
$condominio = new Condominio();
$condominio->pagination = 4;

if(isset($_GET['b'])){
    $filtro = array();

    foreach($_GET['b'] as $field=>$termo){
        switch($field){
            case 'termo1':
                $filtro['nomeCondominio'] = $termo;
                break;
            case 'termo2':
                $filtro['cond.idAdm'] = $termo;
                break;
            default:
                break;
        }
    }
}
$condominio->busca = $filtro;
$result = $condominio->getCondominio();

//total registros
$totalRegistros = ($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'];

//paginacao
$paginacao = ($result['totalResults'] > $condominio->pagination)?$condominio->renderPaginacao($result['qtPaginas']) : '';
?>