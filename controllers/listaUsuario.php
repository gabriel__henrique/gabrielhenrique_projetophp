<?
//busca adm
$usuarios = new Usuario();
$usuarios->pagination = 4;
$usuarios->busca = $_GET['b'];
$result = $usuarios->getUsuarios();

if(isset($_GET['b'])){
    $filtro = array();

    foreach($_GET['b'] as $field=>$termo){
        switch($field){
            case 'termo1':
                $filtro['nome'] = $termo;
                break;
            case 'termo2':
                $filtro['cond.id'] = $termo;
                break;
            default:
                break;
        }
    }
}

//total registro
$totalRegistros = ($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'];

//paginacao
$paginacao = ($result['totalResults'] > $usuarios->pagination)?$usuarios->renderPaginacao($result['qtPaginas']) : '';
?>