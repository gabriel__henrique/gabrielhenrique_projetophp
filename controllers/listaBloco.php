<?
//busca condominio
$condominio = new Condominio();
$listaCondominio = $condominio->getCondominio();
//busca bloco
$blocos = new Bloco();
$blocos->pagination = 4;
if(isset($_GET['b'])){
    $filtro = array();

    foreach($_GET['b'] as $field=>$termo){
        switch($field){
            case 'termo1':
                $filtro['b.nomeBloco'] = $termo;
                break;
            case 'termo2':
                $filtro['b.idCondominio'] = $termo;
                break;
            default:
                break;
        }
    }
}
$blocos->busca = $filtro;
$result = $blocos->getBloco();

//total registros
($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'];

//pagincao
$paginacao = ($result['totalResults'] > $blocos->pagination)?$blocos->renderPaginacao($result['qtPaginas']) : ''
?>