<?
$nav = array(
    'home' => 'Página Incial',
    'lista' => array(
        'listaADM' => 'Administradora',
        'listaCondominio' => 'Condominio',
        'listaConselho'=> 'Conselho',
        'listaBloco' => 'Bloco',
        'listaUnidade' => 'Unidade',
        'listaMorador' => 'Moradores',
    ),
    'Cadastro' => array(
        'cadastroADM' => 'Administradora',
        'cadastroCondominio' => 'Condominio',
        'cadastroConselho' => 'Conselho',
        'cadastroBloco' => 'Bloco',
        'cadastroUnidade' => 'Unidade',
        'cadastroMorador' => 'Morador',
    ),
    'usuarios' => array(
        'cadastroUsuario' => 'cadastrar',
        'listaUsuario' => 'listar'
    )
);
?>