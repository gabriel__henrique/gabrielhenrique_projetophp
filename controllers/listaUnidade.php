<?
//busca condominio
$condominio = new Condominio();
$listaCondominio = $condominio->getCondominio();



//busca unidade
$unidade = new Unidade();
$unidade->pagination = 4;
if(isset($_GET['b'])){
    $filtro = array();

    foreach($_GET['b'] as $field=>$termo){
        switch($field){
            case 'termo1':
                $filtro['unid.numero'] = $termo;
                break;
            case 'termo2':
                $filtro['unid.idCondominio'] = $termo;
                break;
            default:
                break;
        }
    }
}
$unidade->busca = $filtro;
$result = $unidade->getUnidade();

//total registros
$totalRegistros = ($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'];

//paginacao
$paginacao = ($result['totalResults'] > $unidade->pagination)?$unidade->renderPaginacao($result['qtPaginas']) : '';
?>