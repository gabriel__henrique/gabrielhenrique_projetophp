<?
//busca condominio
$condominio = new Condominio();
$listaCondominio = $condominio->getCondominio();


//busca conselho
$conselho = new Conselho();
$conselho->pagination = 4;
if(isset($_GET['b'])){
    $filtro = array();

    foreach($_GET['b'] as $field=>$termo){
        switch($field){
            case 'termo1':
                $filtro['nome'] = $termo;
                break;
            case 'termo2':
                $filtro['idCondominio'] = $termo;
                break;
            default:
                break;
        }
    }
}

$conselho->busca = $filtro;
$result = $conselho->getConselho();

//total registros
$totalRegistros = ($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'];

//paginacao
$paginacao = ($result['totalResults'] > $conselho->pagination)?$conselho->renderPaginacao($result['qtPaginas']) : '';
?>