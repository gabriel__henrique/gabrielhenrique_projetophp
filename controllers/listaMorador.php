<?
//buscar os condominios
$condominio = new Condominio();
$listaCondominio = $condominio->getCondominio();

//busca os moradores
$morador = new Morador();
$morador->pagination = 4;


if(isset($_GET['b'])){
    $filtro = array();

    foreach($_GET['b'] as $field=>$termo){
        switch($field){
            case 'termo1':
                $filtro['nome'] = $termo;
                break;
            case 'termo2':
                $filtro['cond.id'] = $termo;
                break;
            default:
                break;
        }
    }
}
$morador->busca = $filtro;
$result = $morador->getMorador();

//total registros
$totalRegistros = ($result['totalResults'] < 10) ? '0'.$result['totalResults'] : $result['totalResults'];

//paginacao
$paginacao = ($result['totalResults'] > $morador->pagination)?$morador->renderPaginacao($result['qtPaginas']) : '';
?>