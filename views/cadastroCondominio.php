<h1 class="text-center mb-5 text-light">Cadastro de Condominio</h1>
<form action="#" class="formCondominio" method="POST">
  <div class="form-row">
  <div class="form-group col-md-3">
        <select id="inputState" class="form-control" name="idAdm">
          <option value="">selecionar</option>
          <?foreach($listaADM as $nAdm){?>
          <option value="<?=$nAdm['id']?>" <?=($nAdm['id'] == $popular['idAdm'] ? 'selected' : '')?>><?=$nAdm['nomeAdm']?></option>
          <?}?>
        </select>
      </div>
    <div class="form-group col-md-5">
      <input type="text" class="form-control" name="nomeCondominio" value="<?=$popular['nomeCondominio']?>" placeholder="Nome do condominio">
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="qtBlocos" value="<?=$popular['qtBlocos']?>" placeholder="Quantidade Blocos">
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="CEP" value="<?=$popular['CEP']?>" placeholder="CEP">
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-10">
      <input type="text" class="form-control" name="logradouro" value="<?=$popular['logradouro']?>" placeholder="Logradouro">
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="numero" value="<?=$popular['numero']?>"  placeholder="Numero">
    </div>
  </div>

  <div class="form-row">
    <div class="form-group col-md-6">
      <input type="text" class="form-control" name="bairro" value="<?=$popular['bairro']?>" placeholder="Bairro">
    </div>
    <div class="form-group col-md-2">
      <select id="inputState" class="form-control" name="estado">
        <option selected>selecionar</option>
        <?foreach($estados as $ch=>$estado){?>
        <option value="<?=$ch?>" <?=($ch == $popular['estado'] )? 'selected="selected"' : '';?>><?=$estado?></option>
        <?}?>
      </select>
    </div>
    <div class="form-group col-md-4">
      <input type="text" class="form-control" name="cidade" value="<?=$popular['cidade']?>" placeholder="Cidade">
    </div>
  </div>
  <div class="form-group">
        <? if($_GET['id']){?>
            <input type="hidden" name="editar" value="<?=$_GET['id']?>">
        <? } ?>
    </div>
  <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
</form>