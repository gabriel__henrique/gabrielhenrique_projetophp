<?
$all = new Morador();
$totalResult = $all->getTotalRegistros();
?>

<div class="row mb-3">
        <div class="col-md-2 mb-1">
            <a href="?page=listaMorador" class="col-md-12 btn bg-purple">
                Moradores <span class="badge badge-light"><?=($totalResult['resultSet']['qtMoradores'] < 10) ? '0'.$totalResult['resultSet']['qtMoradores'] : $totalResult['resultSet']['qtMoradores']?></span>
            </a>
        </div>
        <div class="col-md-2 mb-1">
            <a href="?page=listaUnidade" class="col-md-12 btn bg-purple">
                Unidades <span class="badge badge-light"><?=($totalResult['resultSet']['qtUnidades'] < 10) ? '0'.$totalResult['resultSet']['qtUnidades'] : $totalResult['resultSet']['qtUnidades']?></span>
            </a>
        </div>
        <div class="col-md-2 mb-1">
            <a href="?page=listaBloco" class="col-md-12 btn bg-purple">
                Bloco <span class="badge badge-light"><?=($totalResult['resultSet']['qtBlocos'] < 10) ? '0'.$totalResult['resultSet']['qtBlocos'] : $totalResult['resultSet']['qtBlocos']?></span>
            </a>
        </div>
        <div class="col-md-2 mb-1">
            <a href="?page=listaCondominio" class="col-md-12 btn bg-purple">
                Condominios <span class="badge badge-light"><?=($totalResult['resultSet']['qtCondominios'] < 10) ? '0'.$totalResult['resultSet']['qtCondominios'] : $totalResult['resultSet']['qtCondominios']?></span>
            </a>
        </div>
        <div class="col-md-2 mb-1">
            <a href="?page=listaADM" class="col-md-12 btn bg-purple">
                Admin. <span class="badge badge-light"><?=($totalResult['resultSet']['qtAdms'] < 10) ? '0'.$totalResult['resultSet']['qtAdms']  :$totalResult['resultSet']['qtAdms'] ?></span>
            </a>
        </div>
</div>
<div class="row">
    <div class="col-md-4">
        <table class="table table-striped table-hover table-bordered table-dark" id="listaUnidadeCondominio">
            <tr>
                <td scope="col" class="text-center">Condominio</td>
                <td scope="col" class="text-center">N° moradores</td>
            </tr>
            <?
            $result = $all->getMoradorporCondominio();
            foreach($result['resultSet'] as $dados){
                ?>
            <tr data-id="<?=$dados['id']?>">
                <td class="text-center"><?=$dados['nomeCondominio']?></td>
                <td class="text-center"><?=$dados['qtMoradores']?></td>
            </tr>
            <? } ?>
        </table>
    </div>
    <div class="col-md-6 px-0">
        <div class="col-md-12">
            <table class="table table-striped table-hover table-bordered table-dark" id="listaUltimasAdm">
            <tr>
                <td scope="col" class="text-center">Administradora</td>
            </tr>
            <?
            $ultmiaAdm = $all->getUltimasAdm();
            foreach($ultmiaAdm['resultSet'] as $dados){
            ?>
            <tr data-id="<?=$dados['id']?>">
                <td class="text-center"><?=$dados['nomeAdm']?></td>
            </tr>
            <? } ?>
        </table>
    </div>
    <div class="col-md-12">
        <table class="table table-striped table-hover table-bordered table-dark" id="listaUltimosCond">
            <tr>
                <td scope="col" class="text-center">Condominios</td>
            </tr>
            <?
            $ultmiosCond = $all->getUltimosCond();
            foreach($ultmiosCond['resultSet'] as $dados){
            ?>
            <tr data-id="<?=$dados['id']?>">
                <td class="text-center"><?=$dados['nomeCondominio']?></td>
            </tr>
            <? } ?>
        </table>
    </div>
</div>
</div>

