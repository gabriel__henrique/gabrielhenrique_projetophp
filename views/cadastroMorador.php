<div class="row">
    <h1 class="col-12 col-sm12 text-center mb-5 text-white">Cadastro De Pessoas</h1>
</div>
<form action="#" class="form-morador" method="POST">
    <div class="form-row">
    <div class="form-group col-md-3">
      <select id="inputState" class="fromCondominio form-control" name="idCondominio">
          <option selected>condominio</option>
          <?foreach($listaCondominio as $nCondominio){?>
            <option value="<?=$nCondominio['id']?>" <?=($nCondominio['id'] == $popular['idCondominio'] ? 'selected' : '')?>><?=$nCondominio['nomeCondominio']?></option>
          <?}?>
        </select>
      </div>
    <div class="form-group col-md-3">
        <select id="inputState" class="fromBloco form-control" name="idBloco">
          <?
          if($_GET['id']){
            $blocos = $morador->getBlocoFromCondominio($popular['idCondominio']);
            foreach($blocos['resultSet'] as $bloco){
              ?>
            <option value="<?=$bloco['id']?>"<?=($bloco['id'] == $popular['idBloco'] ? 'selected' : '')?>><?=$bloco['nomeBloco']?></option>
            <?}}?>
        </select>
      </div>
      <div class="form-group col-md-3">
        <select id="inputState" class="fromUnidade form-control" name="idUnidade">
          <?
          if($_GET['id']){
            $unidades = $morador->getUnidadeFromBloco($popular['idBloco']);
            foreach($unidades['resultSet'] as $unidade){
          ?>
          <option value="<?=$unidade['id']?>"<?=($unidade['id'] == $popular['idUnidade'] ? 'selected' : '')?>><?=$unidade['numero']?></option>
          <?}}?>
        </select>
      </div>
    </div>
    <div class="form-group">
        <input class="form-control" type="text" name="nome" placeholder="Nome" value="<?=$popular['nome']?>" required>
    </div>
    <div class="form-group">
        <input class="form-control" type="text" name="cpf" placeholder="CPF" value="<?=$popular['cpf']?>" required>
    </div>
    <div class="form-group">
        <input class="form-control" type="email" name="email" placeholder="E-mail" value="<?=$popular['email']?>" required>
    </div>
    <div class="form-group">
        <input class="form-control" type="text" name="telefone" placeholder="Telefone" value="<?=$popular['telefone']?>">
    </div>
    <div class="form-group">
        <? if($_GET['id']){?>
            <input type="hidden" name="editar" value="<?=$_GET['id']?>">
        <? } ?>
    </div>
    <button class="btn btn-primary col-12 col-sm-3 buttonEnviar" type="submit">Enviar</button>
</form>