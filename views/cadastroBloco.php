<h1 class="text-center mb-5 text-light">Cadastro de Bloco</h1>
<form action="#" class="formBloco" method="POST">
  <div class="form-row">
    <div class="form-group col-md-3">
        <select id="inputState" class="form-control" name="idCondominio">
          <option value="">selecionar</option>
          <?foreach($listaCondominio as $nCondominio){?>
          <option value="<?=$nCondominio['id']?>" <?=($nCondominio['id'] == $popular['idCond'] ? 'selected' : '')?>><?=$nCondominio['nomeCondominio']?></option>
          <?}?>
        </select>
      </div>
      <div class="form-group col-md-2">
        <input type="text" class="form-control" name="nomeBloco" value="<?=$popular['nomeBloco']?>" placeholder="Nome do Bloco">
      </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="qtAndar" value="<?=$popular['qtAndar']?>" placeholder="Quantidade Andar">
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="qtAptoAndar" value="<?=$popular['qtAptoAndar']?>" placeholder="Apto por Andar">
    </div>
  </div>
  <div class="form-group">
        <? if($_GET['id']){?>
            <input type="hidden" name="editar" value="<?=$_GET['id']?>">
        <? } ?>
    </div>
  <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
</form>