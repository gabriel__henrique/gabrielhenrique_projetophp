<h1 class="text-center mb-5 text-light">Cadastro de Unidade</h1>
<form action="#" class="formUnidade" method="POST">
  <div class="form-row">
  <div class="form-group col-md-3">
        <select id="inputState" class="fromCondominio form-control" name="idCondominio">
          <option>Selecione...</option>
        <?foreach($listaCondominio as $nCondominio){?>
          <option value="<?=$nCondominio['id']?>" <?=($nCondominio['id'] == $popular['idCondominio'] ? 'selected' : '')?>><?=$nCondominio['nomeCondominio']?></option>
          <?}?>
        </select>
      </div>
    <div class="form-group col-md-3">
        <select id="inputState" class="fromBloco form-control" name="idBloco">
        <?
          if($_GET['id']){
            $blocos = $unidade->getBlocoFromCondominio($popular['idCondominio']);
            foreach($blocos['resultSet'] as $bloco){
              ?>
            <option value="<?=$bloco['id']?>"<?=($bloco['id'] == $popular['idBloco'] ? 'selected' : '')?>><?=$bloco['nomeBloco']?></option>
            <?}}?>
        </select>
      </div>
      <div class="form-group col-md-2">
        <input type="text" class="form-control" name="numero" value="<?=$popular['numero']?>" placeholder="numero">
      </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="metragem" value="<?=$popular['metragem']?>" placeholder="metragem">
    </div>
    <div class="form-group col-md-2">
      <input type="text" class="form-control" name="qtVagas" value="<?=$popular['qtVagas']?>" placeholder="Vagas Garagem">
    </div>
  </div>
  <div class="form-group">
        <? if($_GET['id']){?>
            <input type="hidden" name="editar" value="<?=$_GET['id']?>">
        <? } ?>
    </div>
  <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
</form>