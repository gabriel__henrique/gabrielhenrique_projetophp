<h1 class="text-center mb-5">Lista de Morador</h1>
    <div class="filter col-md-12">

    </div>
    <div class="table-responsive">

    <table class="table table-striped table-hover table-bordered table-dark" id="listaMorador">
        <tr>
            <td colspan="10">
            <form class="form-inline my-2 my-lg-0" id="filtro" method="GET">
                <input type="hidden" name="page" value="listaMorador">
                <div class="input-group-prepend">
                    <div class="input-group-text">busca por nome</div>
                </div>
                <input class="form-control mr-sm-2 col-md-3 termo1" type="search" placeholder="Nome" aria-label="Search" name="b[nome]">
                <div class="input-group-prepend">
                    <div class="input-group-text">por condomínio</div>
                </div>
                <select name="b[idCondominio]" class="col-md-3 custom-select mr-5 termo2" >
                    <option value="">Selecione...</option>
                    <?
                    foreach($listaCondominio['resultSet'] as $condominios){
                        echo '<option value="'.$condominios['id'].'">'.$condominios['nomeCondominio'].'</option>';
                    }?>
                </select>
                <button class="btn btn-outline-primary my-2 my-sm-0 ml-3" type="submit" disabled>Buscar</button>
                <a class="btn btn-outline-danger my-2 my-sm-0 ml-3" href="<?=$urlSite?>listaMorador">Limpar</a>
            </form>
            </td>
        </tr>
        <tr>
            <td scope="col" class="text-center">Condominio</td>
            <td scope="col" class="text-center">Bloco</td>
            <td scope="col" class="text-center">Unidade</td>
            <td scope="col" class="text-center">Nome</td>
            <td scope="col" class="text-center">CPF</td>
            <td scope="col" class="text-center">Email</td>
            <td scope="col" class="text-center">Telefone</td>
            <td>DT Cadastro</td>
            <td>DT Update</td>
            <td class="text-center"><a href="<?=$urlSite?>cadastroMorador" class="btn btn-light px-3 py-0"><small class="mr-2">Adicionar</small><i class="bi bi-plus-circle"></i></a></td>
        </tr>
        <?
        foreach($result['resultSet'] as $ch2=>$dados){
        ?>
        <tr data-id="<?=$dados['id']?>">
            <td class="text-center"><?=$dados['nomeCondominio']?></td>
            <td class="text-center"><?=$dados['nomeBloco']?></td>
            <td class="text-center"><?=$dados['numero']?></td>
            <td class="text-center"><?=$dados['nome']?></td>
            <td class="text-center"><?=$dados['cpf']?></td>
            <td class="text-center"><?=$dados['email']?></td>
            <td class="text-center"><?=($dados['telefone']) ? $dados['telefone'] : 'n/a'?></td>
            <td><?=dateFormat($dados['dataCadastro'])?></td> 
            <td><?=dateFormat($dados['dataUpdate'])?></td>  
            <td class="text-center">
                <a href="#" data-id="<?=$dados['id']?>" class="removerMorador text-white mr-5"><i class="bi bi-trash3"></i></a>
                <a href="<?=$urlSite?>cadastroMorador/<?=$dados['id']?>" class=" text-white"><i class="bi bi-pencil-square"></i></a>
            </td>
        </tr>
        <? } ?>
        <tr>
            <td colspan="10" class="text-right">Total Registros <small class="badge badge-light totalRegistro"><?=$totalRegistros?></small></td>
        </tr>
     </table> 
    </div>
     <?=$paginacao?>

