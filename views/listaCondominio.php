<h1 class="text-center">Lista Condominio</h1>
    <table class="table table-striped table-hover table-bordered table-dark" id="listaCondominio">
    <tr>
            <td colspan="10">
            <form class="form-inline my-2 my-lg-0" id="filtro" method="GET">
                <input type="hidden" name="page" value="listaCondominio">
                <div class="input-group-prepend">
                    <div class="input-group-text">busca por nome</div>
                </div>
                <input class="form-control mr-sm-2 col-md-3 termo1" type="search" placeholder="Condominio" aria-label="Search" name="b[nome]">
                <div class="input-group-prepend">
                    <div class="input-group-text">por Administradora</div>
                </div>
                <select name="b[idAdm]" class="termo2 custom-select col-md-3 mr-5">
                    <option value="">Selecione...</option>
                    <?
                    foreach($listaAdm['resultSet'] as $ADM){
                        echo '<option value="'.$ADM['id'].'">'.$ADM['nomeAdm'].'</option>';
                    }?>
                </select>
                <button class="btn btn-outline-primary my-2 my-sm-0 ml-3" type="submit" disabled>Buscar</button>
                <a class="btn btn-outline-danger my-2 my-sm-0 ml-3" href="<?=$urlSite?>listaCondominio">Limpar</a>
            </form>
            </td>
        </tr>
        <tr>
            <td scope="col" class="text-center">Administradora</td>
            <td scope="col" class="text-center">Nome</td>
            <td scope="col" class="text-center">qt.Blocos</td>
            <td scope="col" class="text-center">CEP</td>
            <td scope="col" class="text-center">Logradouro</td>
            <td scope="col" class="text-center">N°</td>
            <td scope="col" class="text-center">Bairro</td>
            <td scope="col" class="text-center">Cidade</td>
            <td scope="col" class="text-center">Estado</td>

            <td class="text-center"><a href="<?=$urlSite?>cadastroCondominio" class="btn btn-light px-3 py-0"><small class="mr-2">Adicionar</small><i class="bi bi-plus-circle"></i></a></td>
        </tr>
        <?
        foreach($result['resultSet'] as $ch2=>$dados){
        ?>
        <tr data-id="<?=$dados['id']?>">
            <td class="text-center"><?=$dados['nomeAdm']?></td>
            <td class="text-center"><?=$dados['nomeCondominio']?></td>
            <td class="text-center"><?=$dados['qtBlocos']?></td>
            <td class="text-center"><?=$dados['cep']?></td>
            <td class="text-center"><?=$dados['logradouro']?></td>
            <td class="text-center"><?=$dados['numero']?></td>
            <td class="text-center"><?=$dados['bairro']?></td>
            <td class="text-center"><?=$dados['cidade']?></td>
            <td class="text-center"><?=$dados['estado']?></td>
            <td class="text-center">
                <a href="#" data-id="<?=$dados['id']?>" class="removerCondominio text-white mr-5"><i class="bi bi-trash3"></i></a>
                <a href="<?=$urlSite?>cadastroCondominio/<?=$dados['id']?>" class=" text-white"><i class="bi bi-pencil-square"></i></a>
            </td>
        </tr>
        <? } ?>
        <tr>
            <td colspan="10" class="text-right">Total Registros <small class="badge badge-light totalRegistro"><?=$totalRegistros?></small></td>
        </tr>
     </table>
     <?=$paginacao?> 