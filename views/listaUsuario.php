<h1 class="text-center">Lista Administradora</h1>
    <table class="table table-striped table-hover table-bordered table-dark" id="listaUsuario">
        <tr>
            <td colspan="10">
            <form class="form-inline my-2 my-lg-0" id="filtro" method="GET">
                <input type="hidden" name="page" value="listaADM">
                <input class="form-control mr-sm-2 col-md-3 termo1" type="search" placeholder="busca por nome" aria-label="Search" name="buscar">
                <button class="btn btn-outline-primary my-2 my-sm-0 ml-3" type="submit" disabled>busca</button>
                <a class="btn btn-outline-danger my-2 my-sm-0 ml-3" href="?page=listaUsuario">Limpar</a>
            </form>
            </td>
        </tr>
        <tr>
            <td scope="col" class="text-center">Nome</td>
            <td scope="col" class="text-center">Usuario</td>
            <td class="text-center"><a href="?page=cadastroUsuario" class="btn btn-light px-3 py-0"><small class="mr-2">Adicionar</small><i class="bi bi-plus-circle"></i></a></td>
        </tr>
        <?
        foreach($result['resultSet'] as $dados){
        ?>
        <tr data-id="<?=$dados['id']?>">
            <td class="text-center"><?=$dados['nome']?></td>
            <td class="text-center"><?=$dados['usuario']?></td>
            <td class="text-center">
                <a href="#" data-id="<?=$dados['id']?>" class="removerUsuario text-white mr-5"><i class="bi bi-trash3"></i></a>
                <a href="?page=cadastroUsuario&id=<?=$dados['id']?>" class=" text-white"><i class="bi bi-pencil-square"></i></a>
            </td>
        </tr>
        <? } ?>
        <tr>
            <td colspan="10" class="text-right">Total Registros <small class="badge badge-light totalRegistro"><?=$totalRegistros?></small></td>
        </tr>
     </table>
     <?=$paginacao?> 