<h1 class="text-center">Lista Unidade</h1>
    <table class="table table-striped table-hover table-bordered table-dark" id="listaUnidade">
    <tr>
            <td colspan="10">
            <form class="form-inline my-2 my-lg-0" id="filtro" method="GET">
                <input type="hidden" name="page" value="listaUnidade">
                <div class="input-group-prepend">
                    <div class="input-group-text">busca por N°</div>
                </div>
                <input class="form-control mr-sm-2 col-md-3 termo1" type="search" placeholder="Numero" aria-label="Search" name="b[numero]">
                <div class="input-group-prepend">
                    <div class="input-group-text">por condomínio</div>
                </div>
                <select name="b[idCondominio]" class="termo2 custom-select col-md-3 mr-5" >
                    <option value="">Selecione...</option>
                    <?
                    foreach($listaCondominio['resultSet'] as $condominios){
                        echo '<option value="'.$condominios['id'].'">'.$condominios['nomeCondominio'].'</option>';
                    }?>
                </select>
                <button class="btn btn-outline-primary my-2 my-sm-0 ml-3" type="submit" disabled>Buscar</button>
                <a class="btn btn-outline-danger my-2 my-sm-0 ml-3" href="<?=$urlSite?>listaUnidade">Limpar</a>
            </form>
            </td>
        </tr>
        <tr>
            <td scope="col" class="text-center">Condominio</td>
            <td scope="col" class="text-center">Bloco</td>
            <td scope="col" class="text-center">numero</td>
            <td scope="col" class="text-center">Metragem</td>
            <td scope="col" class="text-center">qt Vagas na Garagem</td>
            <td class="text-center"><a href="<?=$urlSite?>cadastroUnidade" class="btn btn-light px-3 py-0"><small class="mr-2">Adicionar</small><i class="bi bi-plus-circle"></i></a></td>
        </tr>
        <?     
        foreach($result['resultSet'] as $ch2=>$dados){
        ?>
        <tr data-id="<?=$dados['id']?>">
            <td class="text-center"><?=$dados['nomeCondominio']?></td>
            <td class="text-center"><?=$dados['nomeBloco']?></td>
            <td class="text-center"><?=$dados['numero']?></td>
            <td class="text-center"><?=$dados['metragem']?></td>
            <td class="text-center"><?=$dados['qtVagas']?></td>
            <td class="text-center">
                <a href="#" data-id="<?=$dados['id']?>" class="removerUnidade text-white mr-5"><i class="bi bi-trash3"></i></a>
                <a href="<?=$urlSite?>cadastroUnidade/<?=$dados['id']?>" class=" text-white"><i class="bi bi-pencil-square"></i></a>
            </td>
        </tr>
        <? } ?>
        <tr>
            <td colspan="10" class="text-right">Total Registros <small class="badge badge-light totalRegistro"><?=$totalRegistros?></small></td>
        </tr>
     </table> 
     <?=$paginacao?>