<h1 class="text-center">Lista Conselho</h1>
    <table class="table table-striped table-hover table-bordered table-dark" id="listaConselho">
    <tr>
            <td colspan="10">
            <form class="form-inline my-2 my-lg-0" id="filtro" method="GET">
                <input type="hidden" name="page" value="listaConselho">
                <div class="input-group-prepend">
                    <div class="input-group-text">busca por nome</div>
                </div>
                <input class="form-control mr-sm-2 col-md-3 termo1" type="search" placeholder="Nome" aria-label="Search" name="b[nome]">
                <div class="input-group-prepend">
                    <div class="input-group-text">por condomínio</div>
                </div>
                <select name="b[idCondominio]" class="termo2 custom-select col-md-3 mr-5" >
                    <option value="">Selecione...</option>
                    <?
                    foreach($listaCondominio['resultSet'] as $condominios){
                        echo '<option value="'.$condominios['id'].'">'.$condominios['nomeCondominio'].'</option>';
                    }?>
                </select>
                <button class="btn btn-outline-primary my-2 my-sm-0 ml-3" type="submit" disabled>Buscar</button>
                <a class="btn btn-outline-danger my-2 my-sm-0 ml-3" href="<?=$urlSite?>listaConselho">Limpar</a>
            </form>
            </td>
        </tr>
        <tr>
            <td scope="col" class="text-center">Condominio</td>
            <td scope="col" class="text-center">Nome</td>
            <td scope="col" class="text-center">funcao</td>
            <td>DT Cadastro</td>
            <td>DT Update</td>
            <td class="text-center"><a href="<?=$urlSite?>cadastroConselho" class="btn btn-light px-3 py-0"><small class="mr-2">Adicionar</small><i class="bi bi-plus-circle"></i></a></td>
        </tr>
        <?
        foreach($result['resultSet'] as $ch2=>$dados){
        ?>
        <tr data-id="<?=$dados['id']?>">
            <td class="text-center"><?=$dados['nomeCondominio']?></td>
            <td class="text-center"><?=$dados['Nome']?></td>
            <td class="text-center"><?=$dados['funcao']?></td>
            <td><?=dateFormat($dados['dataCadastro'])?></td> 
            <td><?=dateFormat($dados['dataUpdate'])?></td>
            <td class="text-center">
                <a href="#" data-id="<?=$dados['id']?>" class="removerConselho text-white mr-5"><i class="bi bi-trash3"></i></a>
                <a href="<?=$urlSite?>cadastroConselho/<?=$dados['id']?>" class=" text-white"><i class="bi bi-pencil-square"></i></a>
            </td>
        </tr>
        <? } ?>
        <tr>
            <td colspan="10" class="text-right">Total Registros <small class="badge badge-light totalRegistro"><?=$totalRegistros?></small></td>
        </tr>


     </table> 
     <?=$paginacao?>