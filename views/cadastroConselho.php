<h1 class="text-center mb-5 text-light">Cadastro de Conselho</h1>
<form action="#" class="formConselho" method="POST">
  <div class="form-row">
  <div class="form-group col-md-3">
        <select id="inputState" class="form-control" name="idCondominio">
          <option selected>condominio</option>
          <?foreach($listaCondominio as $nCondominio){?>
            <option value="<?=$nCondominio['id']?>" <?=($nCondominio['id'] == $popular['idCondominio'] ? 'selected' : '')?>><?=$nCondominio['nomeCondominio']?></option>
          <?}?>
        </select>
    </div>
    <div class="form-group col-md-2">
        <input type="text" class="form-control" name="Nome" value="<?=$popular['Nome']?>" placeholder="nome">
    </div>
    <div class="form-group col-md-3">
        <select id="inputState" class="form-control" name="funcao">
          <option selected>função</option>
          <option value="sindico">sindico</option>
          <option value="subsindico">subSindico</option>
          <option value="conselheiro">conselheiro</option>
        </select>
    </div>
  <div class="form-group">
        <? if($_GET['id']){?>
            <input type="hidden" name="editar" value="<?=$_GET['id']?>">
        <? } ?>
    </div>
  <button type="submit" class="btn btn-primary buttonEnviar">Enviar</button>
</form>