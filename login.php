<?
// error_reporting(E_ERROR | E_PARSE);
require "uteis.php"
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css.">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/signin.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Document</title>
</head>

<body class="text-center">

    <main class="container mt-5">
        <form action="<?=$urlSite?>controllers/restrito.php" class="form-signin" method="POST">
            <h1 class="h3 mb-3 font-weight-normal">Faça login</h1>
            <label for="inputEmail" class="sr-only">Email</label>
            <input type="text" id="inputEmail" class="form-control" name="usuario" placeholder="Usuario" required autofocus>
            <label for="inputPassword" class="sr-only">Senha</label>
            <input type="password" id="inputPassword" class="form-control" name="senha" placeholder="Senha" required>
            <div class="checkbox mb-3">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>
            <button class="btn btn-lg bg-purple btn-block" type="submit">Logar</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2017-2021</p>
        </form>
        
        <script src="js/jquery-3.6.0.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/app.js"></script>
        <?if(isset($_GET['msg'])){?>
            <script type="text/javascript">
                $(function(){
                    myAlert('danger','<?=$_GET['msg']?>','main')
                })
                </script>
        <?}?>
</main>
</body>

</html>