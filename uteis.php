<?
error_reporting(0);
session_start();

$localDir = 'gabrielhenrique_projetophp/';
$baseURL = "http://".$_SERVER['HTTP_HOST'].'/';

$urlSite = $baseURL.$localDir;

$fullPath = $_SERVER['DOCUMENT_ROOT'].'/';
$fullPath .= $localDir;

$models = $fullPath.'models/';
$controllers = $fullPath.'controllers/';
$views = $fullPath.'views/';



require $models."connectDB.Class.php";
require $models."dao.Class.php";
require $models."restrito.class.php";
require $models."usuarios.class.php";
require $models."adm.class.php";
require $models."condominio.class.php";
require $models."conselhoFiscal.class.php";
require $models."bloco.class.php";
require $models."unidade.class.php";
require $models."morador.Class.php";

require $controllers."nav.php";
function legivel($var,$width = '250',$height = '400') {
    echo "<pre>";
    if(is_array($var)) {
        print_r($var);
    } else {
        print($var);
    }
    echo "</pre>";
}
$estados = array( 
    "AC" => "Acre", 
    "AL" => "Alagoas", 
    "AM" => "Amazonas", 
    "AP" => "Amapá",
    "BA" => "Bahia",
    "CE" => "Ceará",
    "DF" => "Distrito Federal",
    "ES" => "Espírito Santo",
    "GO" => "Goiás",
    "MA" => "Maranhão",
    "MT" => "Mato Grosso",
    "MS" => "Mato Grosso do Sul",
    "MG" => "Minas Gerais",
    "PA" => "Pará",
    "PB" => "Paraíba",
    "PR" => "Paraná",
    "PE" => "Pernambuco",
    "PI" => "Piauí",
    "RJ" => "Rio de Janeiro",
    "RN" => "Rio Grande do Norte",
    "RO" => "Rondônia",
    "RS" => "Rio Grande do Sul",
    "RR" => "Roraima",
    "SC" => "Santa Catarina",
    "SE" => "Sergipe",
    "SP" => "São Paulo",
    "TO" => "Tocantins"
);
    
function dateFormat($d, $tipo = true){
    if(!$d){
        return 'sem data';
    }
    if($tipo){
        $hora = explode(' ', $d);
        $data = explode('-',$hora[0]);
        
    
        return $data[2].'/'.$data[1].'/'.$data[0].' '.substr($hora[1],0,-3);;
    }else{
        $hora = explode(' ', $d);
        $data = explode('/',$hora[0]);
        
    
        return $data[2].'-'.$data[1].'-'.$data[0].' '.substr($hora[1],0,-3);
    }
} 

function trataErros($errorNum, $errorStr, $errorFile, $errorLine){
    echo $errorNum.' x '.$errorStr.' x '.$errorFile.' x '.$errorLine.' x ';
}

function trataURL($params = array()){
    $url = (isset($_GET['b'])) ? 'busca/' : '';
    foreach($params as $value){
        $url .= $value.'/';
    }
    return $url;
}
function antiinject($var,$quotes=ENT_NOQUOTES,$keeptags=false) {
    //ENT_QUOTES, ENT_NOQUOTES, ENT_COMPAT;
    
    
    if(!is_array($var)){
        $var = stripslashes($var);
        $var = html_entity_decode($var,$quotes,'utf-8');
        if(!$keeptags) {
            $var = strip_tags($var);
        }
        $var = trim($var);
        //$var = utf8_decode($var);
        /**/
        $var = htmlentities($var,$quotes);
        if($keeptags) {
            $var = str_replace('&lt;','<',$var);
            $var = str_replace('&gt;','>',$var);
        }
        /**/
        $var = addslashes($var);
    } else {
        foreach($var as $k=>$ar){
            $var[$k] = antiinject($ar);
        }
    }
    return $var;
}
?>