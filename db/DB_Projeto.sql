-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.22-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para ap_controle
DROP DATABASE IF EXISTS `ap_controle`;
CREATE DATABASE IF NOT EXISTS `ap_controle` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `ap_controle`;

-- Copiando estrutura para tabela ap_controle.administradora
DROP TABLE IF EXISTS `administradora`;
CREATE TABLE IF NOT EXISTS `administradora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeAdm` varchar(255) NOT NULL DEFAULT '',
  `cnpj` varchar(14) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.administradora: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `administradora` DISABLE KEYS */;
INSERT INTO `administradora` (`id`, `nomeAdm`, `cnpj`, `dataCadastro`) VALUES
	(1, 'Escola', '11222333011111', '2022-03-30 12:59:55'),
	(2, 'Proway', '99555666000177', '2022-03-30 13:01:48'),
	(3, 'APD', '66777888000144', '2022-04-01 11:04:27'),
	(6, 'cccc', 'dssa', '2022-04-01 11:07:07'),
	(7, 'vbbb', '1000000', '2022-04-05 08:30:53'),
	(8, 'carlos', '11111', '2022-04-06 11:54:13');
/*!40000 ALTER TABLE `administradora` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.bloco
DROP TABLE IF EXISTS `bloco`;
CREATE TABLE IF NOT EXISTS `bloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCondominio` int(11) NOT NULL DEFAULT 0,
  `nomeBloco` varchar(255) NOT NULL DEFAULT '',
  `qtAndar` int(11) NOT NULL DEFAULT 0,
  `qtAptoAndar` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `chBlocoCondominio` (`idCondominio`),
  CONSTRAINT `chBlocoCondominio` FOREIGN KEY (`idCondominio`) REFERENCES `condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela ap_controle.bloco: ~15 rows (aproximadamente)
/*!40000 ALTER TABLE `bloco` DISABLE KEYS */;
INSERT INTO `bloco` (`id`, `idCondominio`, `nomeBloco`, `qtAndar`, `qtAptoAndar`) VALUES
	(1, 1, 'A', 5, 4),
	(2, 2, 'B', 8, 6),
	(3, 3, 'D', 5, 3),
	(4, 4, 'F', 2, 2),
	(5, 5, 'A', 5, 4),
	(6, 6, 'K', 6, 2),
	(7, 7, 'L', 25, 1),
	(8, 8, 'B', 2, 4),
	(9, 9, 'C', 8, 3),
	(10, 10, 'R', 4, 4),
	(11, 11, 'T', 12, 2),
	(12, 12, 'O', 23, 1),
	(21, 3, 'aaaaa', 3, 0),
	(22, 7, '', 0, 0),
	(23, 8, 'dd', 8, 8);
/*!40000 ALTER TABLE `bloco` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.condominio
DROP TABLE IF EXISTS `condominio`;
CREATE TABLE IF NOT EXISTS `condominio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idAdm` int(11) NOT NULL DEFAULT 0,
  `nomeCondominio` varchar(255) NOT NULL DEFAULT '',
  `qtBlocos` int(11) NOT NULL DEFAULT 0,
  `cep` varchar(8) NOT NULL DEFAULT '0',
  `logradouro` varchar(255) NOT NULL DEFAULT '',
  `numero` varchar(8) NOT NULL DEFAULT '0',
  `bairro` varchar(255) NOT NULL DEFAULT '',
  `cidade` varchar(255) NOT NULL DEFAULT '',
  `estado` varchar(2) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chCondominioADM` (`idAdm`),
  CONSTRAINT `chCondominioADM` FOREIGN KEY (`idAdm`) REFERENCES `administradora` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.condominio: ~18 rows (aproximadamente)
/*!40000 ALTER TABLE `condominio` DISABLE KEYS */;
INSERT INTO `condominio` (`id`, `idAdm`, `nomeCondominio`, `qtBlocos`, `cep`, `logradouro`, `numero`, `bairro`, `cidade`, `estado`, `dataCadastro`) VALUES
	(1, 1, 'Alohomora', 4, '89080552', 'hogwats', '2222', 'ingla', 'londres', 'SC', '2022-04-06 13:56:49'),
	(2, 1, 'Hogwats', 8, '89086552', 'castelo branco', '3333', 'sla', 'indaial', 'SC', '2022-04-06 13:56:49'),
	(3, 1, 'Tsushima', 5, '1111111', 'ilhados', '999', 'mar', 'navegantes', 'SC', '2022-04-06 13:56:49'),
	(4, 2, 'computador', 2, '89130000', '7 de setembro', '12', 'centro', 'timbo', 'SP', '2022-04-06 13:56:49'),
	(5, 2, 'mouse', 3, '85480999', '15 de novembro', '333', 'carijos', 'timbo', 'SP', '2022-04-06 13:56:49'),
	(6, 2, 'teclado', 2, '87680444', '9 de janeiro', '888', 'warnow', 'indaial', 'SC', '2022-04-06 13:56:49'),
	(7, 1, 'Rar', 4, '78965000', 'agostinho', '1000', 'tapajos', 'ibituba', 'SC', '2022-04-06 13:56:49'),
	(8, 3, 'tela', 4, '87584123', 'Rio branco', '212', 'estrada das areias', 'rodeio', 'RS', '2022-04-06 13:56:49'),
	(9, 3, 'fone', 2, '65498777', 'Pedrinho', '555', 'polaquia', 'Dr.Pedrinho', 'RS', '2022-04-06 05:55:09'),
	(10, 2, 'microfone', 1, '58480700', 'piratuba', '321', 'estados', 'witmarsun', 'AC', '2022-04-06 06:00:09'),
	(11, 3, 'pendrive', 5, '75391564', 'alagoas', '465', 'polaquia', 'timbo', 'AL', '2022-04-06 13:56:49'),
	(12, 1, '7777777777', 4, '5555', '', '', '', '', 'se', '2022-04-06 13:56:49'),
	(40, 1, '99999999', 0, '5555', 'addadadas', '555', 'dadad', 'dadasd', 'AL', '2022-04-06 11:00:00'),
	(50, 1, 'grava', 0, '', '', '', '', '', 'se', '2022-04-06 13:56:49'),
	(51, 1, 'tes', 0, '', '', '', '', '', 'se', '2022-04-06 12:00:00'),
	(52, 1, '7777777777', 0, '', '', '', '', '', 'se', '2022-04-06 13:56:49'),
	(53, 6, 'teste', 0, '', '', '', '', '', 'se', '2022-04-06 13:56:49'),
	(54, 8, 'nao sei', 4, '5555', 'rua alamade weed', '555', 'cone', 'crew', 'MA', '2022-04-06 14:17:39');
/*!40000 ALTER TABLE `condominio` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.conselho
DROP TABLE IF EXISTS `conselho`;
CREATE TABLE IF NOT EXISTS `conselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCondominio` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `funcao` enum('sindico','subsindico','conselheiro') NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `chConselhoCondominio` (`idCondominio`),
  CONSTRAINT `chConselhoCondominio` FOREIGN KEY (`idCondominio`) REFERENCES `condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela ap_controle.conselho: ~54 rows (aproximadamente)
/*!40000 ALTER TABLE `conselho` DISABLE KEYS */;
INSERT INTO `conselho` (`id`, `idCondominio`, `nome`, `funcao`) VALUES
	(2, 1, 'Doombladore', 'subsindico'),
	(3, 1, 'Ron', 'conselheiro'),
	(4, 1, 'harry', 'conselheiro'),
	(6, 2, 'wonka', 'subsindico'),
	(7, 2, 'snape', 'conselheiro'),
	(8, 2, 'umbrigde', 'conselheiro'),
	(9, 2, 'maa', 'conselheiro'),
	(11, 3, 'brian', 'conselheiro'),
	(13, 4, 'rogers', 'subsindico'),
	(14, 3, 'diogo', 'subsindico'),
	(15, 4, 'stan', 'conselheiro'),
	(17, 5, 'viuva', 'subsindico'),
	(18, 5, 'falcao', 'conselheiro'),
	(19, 5, 'negro', 'conselheiro'),
	(20, 6, 'aaa', 'sindico'),
	(21, 6, 'gabriel', 'subsindico'),
	(22, 6, 'flucas', 'conselheiro'),
	(23, 6, 'gridnaldo', 'conselheiro'),
	(24, 6, 'spiao', 'conselheiro'),
	(25, 7, 'felix', 'sindico'),
	(26, 7, 'maria', 'subsindico'),
	(27, 7, 'joao', 'conselheiro'),
	(28, 7, 'judas', 'conselheiro'),
	(30, 7, 'jesus', 'conselheiro'),
	(32, 8, 'tomas', 'subsindico'),
	(33, 8, 'trem', 'conselheiro'),
	(34, 8, 'the rok', 'conselheiro'),
	(35, 8, 'Keynzewo', 'conselheiro'),
	(37, 9, 'Nahteifo', 'subsindico'),
	(38, 9, 'Othguo', 'conselheiro'),
	(39, 9, 'Isvyu', 'conselheiro'),
	(40, 9, 'Dahbu', 'conselheiro'),
	(42, 10, 'Fuazermi', 'subsindico'),
	(43, 10, 'Drogdu', 'conselheiro'),
	(44, 10, 'Hifiru', 'conselheiro'),
	(45, 10, 'Raotohu', 'conselheiro'),
	(46, 11, 'Beynal', 'sindico'),
	(47, 11, 'Palo', 'subsindico'),
	(48, 11, 'Thiarnfe', 'conselheiro'),
	(49, 11, 'Luftar', 'conselheiro'),
	(50, 11, 'Zuir', 'conselheiro'),
	(54, 12, 'Gilpauor', 'conselheiro'),
	(55, 12, 'Oruil', 'conselheiro'),
	(61, 3, 'Porion', 'conselheiro'),
	(79, 40, 'zezin', 'conselheiro'),
	(80, 40, 'aaa', 'conselheiro'),
	(81, 40, 'ds', 'conselheiro'),
	(82, 40, 'gravou', 'conselheiro'),
	(84, 12, 'maria', 'subsindico'),
	(85, 52, 'bbb', 'conselheiro'),
	(86, 40, 'maria', 'sindico'),
	(87, 40, 'zezin', 'subsindico');
/*!40000 ALTER TABLE `conselho` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.listaconvidado
DROP TABLE IF EXISTS `listaconvidado`;
CREATE TABLE IF NOT EXISTS `listaconvidado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idReserva` int(11) NOT NULL,
  `idUnidade` int(11) NOT NULL,
  `convidado` varchar(255) NOT NULL DEFAULT '',
  `cpfConvidado` varchar(11) NOT NULL DEFAULT '',
  `telefoneConvidado` varchar(14) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `chConvidadoUnidade` (`idUnidade`),
  KEY `chConvidadoReserva` (`idReserva`),
  CONSTRAINT `chConvidadoReserva` FOREIGN KEY (`idReserva`) REFERENCES `reserva_salao_festa` (`id`),
  CONSTRAINT `chConvidadoUnidade` FOREIGN KEY (`idUnidade`) REFERENCES `unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.listaconvidado: ~27 rows (aproximadamente)
/*!40000 ALTER TABLE `listaconvidado` DISABLE KEYS */;
INSERT INTO `listaconvidado` (`id`, `idReserva`, `idUnidade`, `convidado`, `cpfConvidado`, `telefoneConvidado`) VALUES
	(2, 1, 3, 'jao', '65413298755', '47985476521'),
	(3, 1, 3, 'william', '54685232165', '41985476521'),
	(4, 1, 3, 'luis', '21354687991', '11965482315'),
	(5, 2, 3, 'flucas', '58132469877', '47936528479'),
	(6, 2, 3, 'grinaldo', '25687491366', '47923561487'),
	(7, 2, 3, 'felix', '23695147849', '41976451832'),
	(8, 3, 3, 'diogo', '91528674354', '47956321478'),
	(9, 3, 3, 'gabriel', '82694627631', '47991545236'),
	(10, 3, 3, 'jonatan', '56421378945', '47255678941'),
	(11, 4, 14, 'leticia', '85467895466', '47998456432'),
	(12, 4, 14, 'alanis', '44566512355', '47566123578'),
	(13, 4, 14, 'leticia', '12356487841', '41325655548'),
	(14, 5, 14, 'henrique', '14651246148', '11012163545'),
	(15, 5, 14, 'franisco', '46521845214', '13215464897'),
	(16, 5, 14, 'carlos', '45642317465', '41544123154'),
	(17, 6, 14, 'hyago', '89784564525', '32101562168'),
	(18, 6, 14, 'nayara', '56421858751', '00132016522'),
	(19, 6, 14, 'delane', '87651211402', '12016412548'),
	(20, 7, 6, 'ney', '79846519842', '01361655142'),
	(21, 7, 6, 'carla', '98716546165', '54621685421'),
	(22, 7, 6, 'djalma', '16855210546', '95142306454'),
	(23, 8, 6, 'araceli', '45642554886', '54654213478'),
	(24, 8, 6, 'junior', '87951432164', '45411345145'),
	(25, 8, 6, 'emily', '87465415648', '23126478425'),
	(26, 9, 6, 'samuel', '87251654565', '45642346548'),
	(27, 9, 6, 'marcelo', '81236.47958', '45632168745'),
	(28, 9, 6, 'suelin', '21684625458', '95626354846');
/*!40000 ALTER TABLE `listaconvidado` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.morador
DROP TABLE IF EXISTS `morador`;
CREATE TABLE IF NOT EXISTS `morador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCondominio` int(11) NOT NULL DEFAULT 0,
  `idBloco` int(11) NOT NULL DEFAULT 0,
  `idUnidade` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL DEFAULT '0',
  `telefone` varchar(11) DEFAULT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `chMoradorBloco` (`idBloco`),
  KEY `chMoradorUnidade` (`idUnidade`),
  KEY `chMoradorCondominio` (`idCondominio`),
  CONSTRAINT `chMoradorBloco` FOREIGN KEY (`idBloco`) REFERENCES `bloco` (`id`),
  CONSTRAINT `chMoradorCondominio` FOREIGN KEY (`idCondominio`) REFERENCES `condominio` (`id`),
  CONSTRAINT `chMoradorUnidade` FOREIGN KEY (`idUnidade`) REFERENCES `unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela ap_controle.morador: ~45 rows (aproximadamente)
/*!40000 ALTER TABLE `morador` DISABLE KEYS */;
INSERT INTO `morador` (`id`, `idCondominio`, `idBloco`, `idUnidade`, `nome`, `cpf`, `email`, `telefone`, `dataCadastro`) VALUES
	(1, 1, 1, 1, 'harry', '00000000000', 'aaaaa@aaaa', '47999999999', '2022-03-30 11:04:43'),
	(2, 3, 3, 4, 'ggg', '11111111111', 'sdad@fdf', '47888888888', '2022-03-30 13:52:19'),
	(3, 2, 2, 3, 'ron', '00000000000', 'aaaaa@aaaa', '47999999999', '2022-03-30 13:52:00'),
	(4, 4, 4, 6, 'herm', '00000000000', 'aaaaa@aaaa', '47999999999', '2022-03-30 13:52:48'),
	(5, 5, 5, 7, 'hagrid', '00000000000', 'aaaaa@aaaa', '47999999999', '2022-03-30 13:52:51'),
	(6, 6, 6, 8, 'dra', '00000000000', 'aaaaa@aaaa', '47999999999', '2022-03-30 13:52:53'),
	(7, 7, 7, 9, 'hert', '12345678910', 'bbb@bbb', NULL, '2022-03-30 13:52:55'),
	(9, 9, 9, 11, 'jin', '25614897366', 'ddd@ddd', NULL, '2022-03-30 13:53:00'),
	(14, 40, 12, 4, 'fd', '777', 'gabriel.hamazonas@gmail.com', '222', '2022-04-01 10:38:37'),
	(15, 8, 8, 10, 'zezin', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:43:35'),
	(16, 9, 9, 11, 'maria', '000-000-000', 'asdsa@dsasd', '222', '2022-04-04 11:43:56'),
	(17, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(18, 1, 1, 1, 'harry', '00000000000', 'aaaaa@aaaa', '47999999999', '2022-03-30 11:04:43'),
	(19, 1, 1, 1, 'harry', '00000000000', 'aaaaa@aaaa', '47999999999', '2022-03-30 11:04:43'),
	(20, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(21, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(22, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(23, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(24, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(25, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(26, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(27, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(28, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(29, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(30, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(31, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(32, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(33, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(34, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(35, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(36, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(37, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(38, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(39, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(40, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(41, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(42, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(43, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(44, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(45, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(46, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(47, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(48, 7, 7, 9, 'fd', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-04 11:44:24'),
	(51, 8, 8, 10, 'zezin', '000-000-000', 'gabriel.hamazonas@gmail.com', '222', '2022-04-07 14:46:44');
/*!40000 ALTER TABLE `morador` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.pets
DROP TABLE IF EXISTS `pets`;
CREATE TABLE IF NOT EXISTS `pets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomePet` varchar(255) NOT NULL DEFAULT '',
  `tipo` enum('cachorro','gato','passaro') NOT NULL,
  `idMorador` int(11) NOT NULL DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_pets_morador` (`idMorador`),
  CONSTRAINT `FK_pets_morador` FOREIGN KEY (`idMorador`) REFERENCES `morador` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela ap_controle.pets: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `pets` DISABLE KEYS */;
INSERT INTO `pets` (`id`, `nomePet`, `tipo`, `idMorador`, `dataCadastro`) VALUES
	(1, 'harry', 'passaro', 1, '2022-03-30 11:30:10'),
	(2, 'ggg', 'gato', 1, '2022-03-30 11:30:12'),
	(3, 'ron', 'cachorro', 3, '2022-03-30 11:30:20'),
	(4, 'herm', 'cachorro', 3, '2022-03-30 11:30:25'),
	(5, 'hagrid', 'passaro', 6, '2022-03-30 11:30:35'),
	(6, 'dra', 'passaro', 6, '2022-03-30 11:30:34');
/*!40000 ALTER TABLE `pets` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.reserva_salao_festa
DROP TABLE IF EXISTS `reserva_salao_festa`;
CREATE TABLE IF NOT EXISTS `reserva_salao_festa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUnidade` int(11) NOT NULL,
  `tituloEvento` varchar(255) NOT NULL,
  `dataHoraEvento` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chReservaUnidade` (`idUnidade`),
  CONSTRAINT `chReservaUnidade` FOREIGN KEY (`idUnidade`) REFERENCES `unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.reserva_salao_festa: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `reserva_salao_festa` DISABLE KEYS */;
INSERT INTO `reserva_salao_festa` (`id`, `idUnidade`, `tituloEvento`, `dataHoraEvento`, `dataCadastro`) VALUES
	(1, 3, 'festa 15 anos', '2022-02-25 14:50:30', '2022-03-30 14:50:59'),
	(2, 3, 'social', '2022-03-30 18:00:00', '2022-03-30 14:51:31'),
	(3, 3, 'AA', '2022-05-26 14:15:00', '2022-03-30 14:52:10'),
	(4, 14, 'exposicao Arte', '2022-06-23 19:00:00', '2022-03-30 14:52:44'),
	(5, 14, 'noite de Jogos', '2021-02-11 20:00:00', '2022-03-30 14:53:24'),
	(6, 14, 'aula sql', '2022-03-30 14:53:58', '2022-03-30 14:54:02'),
	(7, 6, 'jantar', '2022-07-20 16:00:00', '2022-03-30 14:54:37'),
	(8, 6, 'cha de cozinha', '2021-10-22 22:00:00', '2022-03-30 14:55:14'),
	(9, 6, 'cafe com amigos', '2020-12-01 15:00:00', '2022-03-30 14:56:03');
/*!40000 ALTER TABLE `reserva_salao_festa` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.unidade
DROP TABLE IF EXISTS `unidade`;
CREATE TABLE IF NOT EXISTS `unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idCondominio` int(11) NOT NULL DEFAULT 0,
  `idBloco` int(11) NOT NULL DEFAULT 0,
  `numero` int(11) NOT NULL DEFAULT 0,
  `metragem` int(11) NOT NULL DEFAULT 0,
  `qtVagas` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `chUnidadeCondominio` (`idCondominio`),
  KEY `chUnidadeBloco` (`idBloco`),
  CONSTRAINT `chUnidadeBloco` FOREIGN KEY (`idBloco`) REFERENCES `bloco` (`id`),
  CONSTRAINT `chUnidadeCondominio` FOREIGN KEY (`idCondominio`) REFERENCES `condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela ap_controle.unidade: ~14 rows (aproximadamente)
/*!40000 ALTER TABLE `unidade` DISABLE KEYS */;
INSERT INTO `unidade` (`id`, `idCondominio`, `idBloco`, `numero`, `metragem`, `qtVagas`) VALUES
	(1, 1, 1, 101, 35, 2),
	(2, 1, 1, 403, 36, 2),
	(3, 2, 2, 202, 40, 4),
	(4, 3, 3, 505, 45, 3),
	(5, 1, 1, 303, 36, 3),
	(6, 4, 4, 202, 75, 5),
	(7, 5, 5, 302, 45, 2),
	(8, 6, 6, 405, 65, 3),
	(9, 7, 7, 805, 65, 4),
	(10, 8, 8, 102, 40, 2),
	(11, 9, 9, 302, 50, 2),
	(12, 10, 10, 102, 55, 6),
	(13, 11, 11, 1101, 90, 5),
	(14, 12, 12, 1205, 85, 2);
/*!40000 ALTER TABLE `unidade` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.usuarios
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT '0',
  `usuario` varchar(255) DEFAULT '0',
  `senha` varchar(255) DEFAULT '0',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.usuarios: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `nome`, `usuario`, `senha`, `dataCadastro`) VALUES
	(22, 'Gabriel', 'Gabriel', '81dc9bdb52d04dc20036dbd8313ed055', '2022-04-07 07:44:42');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

-- Copiando estrutura para view ap_controle.vw_blocos
DROP VIEW IF EXISTS `vw_blocos`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_blocos` (
	`id` INT(11) NOT NULL,
	`idCond` INT(11) NOT NULL,
	`nomeCondominio` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`nomeBloco` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`qtAndar` INT(11) NOT NULL,
	`qtAptoAndar` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view ap_controle.vw_condominio
DROP VIEW IF EXISTS `vw_condominio`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_condominio` (
	`id` INT(11) NOT NULL,
	`idAdm` INT(11) NOT NULL,
	`nomeAdm` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`nomeCondominio` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`qtBlocos` INT(11) NOT NULL,
	`cep` VARCHAR(8) NOT NULL COLLATE 'utf8mb4_general_ci',
	`logradouro` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`numero` VARCHAR(8) NOT NULL COLLATE 'utf8mb4_general_ci',
	`bairro` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`cidade` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`estado` VARCHAR(2) NOT NULL COLLATE 'utf8mb4_general_ci'
) ENGINE=MyISAM;

-- Copiando estrutura para view ap_controle.vw_conselho
DROP VIEW IF EXISTS `vw_conselho`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_conselho` (
	`id` INT(11) NOT NULL,
	`idCondominio` INT(11) NULL,
	`nomeCondominio` VARCHAR(255) NULL COLLATE 'utf8mb4_general_ci',
	`Nome` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`funcao` ENUM('sindico','subsindico','conselheiro') NOT NULL COLLATE 'utf8mb4_general_ci'
) ENGINE=MyISAM;

-- Copiando estrutura para view ap_controle.vw_morador
DROP VIEW IF EXISTS `vw_morador`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_morador` (
	`id` INT(11) NOT NULL,
	`idCondominio` INT(11) NOT NULL,
	`idBloco` INT(11) NOT NULL,
	`idUnidade` INT(11) NOT NULL,
	`nomeCondominio` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`nomeBloco` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`numero` INT(11) NOT NULL,
	`nome` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`cpf` VARCHAR(11) NOT NULL COLLATE 'utf8mb4_general_ci',
	`email` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`telefone` VARCHAR(11) NULL COLLATE 'utf8mb4_general_ci',
	`dataCadastro` TIMESTAMP NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view ap_controle.vw_reservas
DROP VIEW IF EXISTS `vw_reservas`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_reservas` (
	`tituloEvento` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`numeroProprietario` INT(11) NOT NULL,
	`nomeProprietario` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`convidados` MEDIUMTEXT NULL COLLATE 'utf8mb4_general_ci',
	`dataHoraEvento` TIMESTAMP NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view ap_controle.vw_unidade
DROP VIEW IF EXISTS `vw_unidade`;
-- Criando tabela temporária para evitar erros de dependência de VIEW
CREATE TABLE `vw_unidade` (
	`id` INT(11) NOT NULL,
	`idCondominio` INT(11) NOT NULL,
	`idBloco` INT(11) NOT NULL,
	`nomeCondominio` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`nomeBloco` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_general_ci',
	`numero` INT(11) NOT NULL,
	`metragem` INT(11) NOT NULL,
	`qtVagas` INT(11) NOT NULL
) ENGINE=MyISAM;

-- Copiando estrutura para view ap_controle.vw_blocos
DROP VIEW IF EXISTS `vw_blocos`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_blocos`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_blocos` AS SELECT
b.id,
cond.id AS idCond,
cond.nomeCondominio,
b.nomeBloco,
b.qtAndar,
b.qtAptoAndar
FROM bloco b
INNER JOIN condominio cond ON cond.id = b.idCondominio ;

-- Copiando estrutura para view ap_controle.vw_condominio
DROP VIEW IF EXISTS `vw_condominio`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_condominio`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_condominio` AS SELECT
cond.id,
adm.id AS idAdm,
adm.nomeAdm,
cond.nomeCondominio,
cond.qtBlocos,
cond.cep,
cond.logradouro,
cond.numero,
cond.bairro,
cond.cidade,
cond.estado
FROM condominio cond
INNER JOIN administradora adm ON adm.id = cond.idAdm 
ORDER BY adm.nomeAdm asc ;

-- Copiando estrutura para view ap_controle.vw_conselho
DROP VIEW IF EXISTS `vw_conselho`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_conselho`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_conselho` AS SELECT
con.id,
cond.id AS idCondominio,
cond.nomeCondominio,
con.nome AS Nome,
con.funcao AS funcao
-- (SELECT GROUP_CONCAT(' ',conselho.nome) FROM conselho WHERE FIND_IN_SET('conselheiro',conselho.funcao) AND conselho.idCondominio = cond.id) AS conselheiro
FROM conselho con
LEFT JOIN condominio cond ON cond.id = con.idCondominio

 
ORDER BY cond.nomeCondominio, con.funcao ASC ;

-- Copiando estrutura para view ap_controle.vw_morador
DROP VIEW IF EXISTS `vw_morador`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_morador`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_morador` AS SELECT 
mor.id,
mor.idCondominio,
mor.idBloco,
mor.idUnidade,
cond.nomeCondominio,
b.nomeBloco,
unid.numero,
mor.nome,
mor.cpf,
mor.email,
mor.telefone,
mor.dataCadastro
FROM morador mor
INNER JOIN condominio cond ON cond.id = mor.idCondominio
INNER JOIN bloco b ON b.id = mor.idBloco
INNER JOIN unidade unid ON unid.id = mor.idUnidade ;

-- Copiando estrutura para view ap_controle.vw_reservas
DROP VIEW IF EXISTS `vw_reservas`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_reservas`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_reservas` AS SELECT 
reservas.tituloEvento,
unid.numero AS numeroProprietario,
mor.nome AS nomeProprietario,
(SELECT GROUP_CONCAT(' ',listaconvidado.convidado) FROM listaconvidado WHERE listaconvidado.idReserva = reservas.id) AS convidados,
reservas.dataHoraEvento
FROM reserva_salao_festa reservas
INNER JOIN unidade unid ON unid.id = reservas.idUnidade
INNER JOIN morador mor ON mor.idUnidade = unid.id
-- filtro data
-- WHERE reservas.dataHoraEvento BETWEEN '2022-02' AND CURDATE()

-- filtro por evento
-- WHERE reservas.tituloEvento LIKE '%festa%' ;

-- Copiando estrutura para view ap_controle.vw_unidade
DROP VIEW IF EXISTS `vw_unidade`;
-- Removendo tabela temporária e criando a estrutura VIEW final
DROP TABLE IF EXISTS `vw_unidade`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `vw_unidade` AS SELECT
unid.id,
unid.idCondominio,
unid.idBloco,
cond.nomeCondominio,
b.nomeBloco ,
unid.numero,
unid.metragem,
unid.qtVagas
FROM unidade unid
INNER JOIN condominio cond ON cond.id = unid.idCondominio
INNER JOIN bloco b ON b.id = unid.idBloco 
ORDER BY cond.nomeCondominio asc ;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
