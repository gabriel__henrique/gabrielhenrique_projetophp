<?
require "../uteis.php";


$bloco = new Bloco();
if($bloco->deletarBloco($_POST['id'])){
    
    $totalRegistro = $bloco->getBloco()['totalResults'];

    $result = array(
        "status" => 'success',
        "totalRegistro" => ($totalRegistro < 10 ? '0'.$totalRegistro : $totalRegistro),
        "msg" => "Parabens, seu registro foi deletado",
    );
    echo json_encode($result);
}else{
    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser deletado"

    );
    echo json_encode($result);
}
?>