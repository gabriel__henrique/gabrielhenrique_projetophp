<?
require "../uteis.php";


$adm = new Adm();
if($adm->deletarAdministradora($_POST['id'])){
    
    $totalRegistros = $adm->getAdministradora()['totalResults'];

    $result = array(
        "status" => 'success',
        "totalRegistro" => ($totalRegistro < 10 ? '0'.$totalRegistro : $totalRegistro),
        "msg" => "Parabens, seu registro foi deletado",
    );
    echo json_encode($result);
}else{
    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser deletado"

    );
}
?>