<?
require "../uteis.php";


$adm = new Adm();

if($adm->addAdministradora($_POST)){
    $result = array(
        "status" => 'success',
        "msg" => "Registro foi inserido com sucesso",
    );
    echo json_encode($result);
}else{
    $result = array(

        "status" => 'danger',
        "msg" => "Registro nao pode ser inserido",
    );
    echo json_encode($result);
}

?>