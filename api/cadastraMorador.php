<?
require "../uteis.php";


$morador = new Morador();
if($morador->addMorador($_POST)){
    $result = array(

        "status" => 'success',
        "msg" => "Registro foi inserido com sucesso",
    );

    echo json_encode($result);
}else{
    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser inserido"

    );
    echo json_encode($result);
}
?>