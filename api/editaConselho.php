<?
require "../uteis.php";
$conselho = new Conselho();

if($conselho->editarConselho($_POST)){
    $result = array(
        "status" => "success",
        "msg" => "Registro foi editado com sucesso",
    );
    echo json_encode($result);
}else{
    $result = array(

        "status" => 'danger',
        "msg" => "Registro nao pode ser editado",
    );
    echo json_encode($result);
}

?>