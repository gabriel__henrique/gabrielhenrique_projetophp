<?
require "../uteis.php";


$condominio = new Condominio();
if($condominio->deletarCondominio($_POST['id'])){
    $totalRegistros = $condominio->getCondominio()['totalResults'];
    $result = array(
        "status" => 'success',
        "totalRegistros" => ($totalRegistros < 10 ? '0'.$totalRegistros : $totalRegistros),
        "msg" => "Parabens, seu registro foi deletado",
    );
    echo json_encode($result);
}else{
    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser deletado"

    );
    echo json_encode($result);
}
?>