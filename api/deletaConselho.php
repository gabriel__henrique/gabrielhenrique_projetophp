<?
require "../uteis.php";


$conselho = new Conselho();
if($conselho->deletarConselho($_POST['id'])){
    
    $totalRegistros = $conselho->getConselho()['totalResults'];

    $result = array(
        "status" => 'success',
        "totalRegistro" => ($totalRegistro < 10 ? '0'.$totalRegistro : $totalRegistro),
        "msg" => "Parabens, seu registro foi deletado",
    );
    echo json_encode($result);
}else{
    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser deletado"

    );
    echo json_encode($result);
}
?>