<?
require "../uteis.php";


$usuarios= new Usuario();
if($usuarios->deletarUsuarios($_POST['id'])){
    
    $totalRegistros = $usuarios->getUsuarios()['totalResults'];

    $result = array(
        "status" => 'success',
        "totalRegistro" => ($totalRegistro < 10 ? '0'.$totalRegistro : $totalRegistro),
        "msg" => "Parabens, seu registro foi deletado",
    );
    echo json_encode($result);
}else{
    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser deletado"

    );
}
?>