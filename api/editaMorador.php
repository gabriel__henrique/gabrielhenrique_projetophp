<?
require "../uteis.php";


$morador = new Morador();
if($morador->editarMorador($_POST)){
    $result = array(

        "status" => 'success',
        "msg" => "Registro foi editado com sucesso",
    );

    echo json_encode($result);
}else{
    $result = array(
        "status" => 'danger',
        "msg" => "O registro não pode ser editado"

    );
    echo json_encode($result);
}
?>